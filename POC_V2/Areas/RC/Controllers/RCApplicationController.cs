﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dal.RC_DAL;
using dal;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace POC_V2.Areas.RC.Controllers
{
    public class RCApplicationController : Controller
    {
        // GET: RC/RCApplication
        public List<T210_States> ListStates(string Country_Code)
        {
            List<T210_States> ListStates = new List<T210_States>();
            RCApp_DAL obj = new RCApp_DAL();
            try
            {

                ListStates = obj.getStates(Country_Code);
            }
            catch (Exception)
            {

                throw;
            }
            return ListStates;
        }
        [HttpGet]
        public ActionResult RCApp()
        {

            RCApp_DAL obj = new RCApp_DAL();

            ViewBag.Countries = obj.getCountries();
            ViewBag.GetRegularActivities = obj.GetRegularActivities();
            ViewBag.GetStudyActivities = obj.GetStudyActivities();
            ViewBag.GetDataManagementActivities = obj.GetDataManagementActivities();
            ViewBag.GetPharmacyActivities = obj.GetPharmacyActivities();
            ViewBag.GetMedicalSpecialties = obj.GetMedicalSpecialties();
            return View();
        }

        public bool SaveRcApp(T001_Research_Collaborator T001)
        {
            try
            {
             
                RCApp_DAL obj = new RCApp_DAL();
                obj.SaveRcApp(T001);
            }
            catch (Exception ex)
            {

                throw;
            }
            return true;
        }

        public JsonResult GetNxtRCID()
        {
            RCApp_DAL obj = new RCApp_DAL();
            string RCID = obj.GetNxtRCID();
            if (!string.IsNullOrEmpty(RCID))
                return Json(new { RCID = RCID, res = "Success" });
            else
                return Json(new { res = "failed" });

        }
        public JsonResult IsRCIDExists(string RCID)
        {
            RCApp_DAL obj = new RCApp_DAL();
            bool isExist = obj.IsRCIDExists(RCID);            
            return Json(new {res = isExist });            
        }
    }
 
}