﻿using System.Web.Mvc;

namespace POC_V2.Areas.RC
{
    public class RCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RC_default",
                "RC/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}