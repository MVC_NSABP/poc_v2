﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Web.Security;
using dal;
using dal.RC_DAL;

namespace POC_V2.Controllers
{

    public class UserLoginController : Controller
    {
       
        public ActionResult LoginPage()
        {
            RCApp_DAL obj = new RCApp_DAL();
           var country= obj.getCountries();

            //if (UserName == "Admin" && PWD == "nsabp")
            //{

            //    return RedirectToAction("RC_Create", "RC");
            //}           
            //else
            //{
            //    ViewBag.Message = "Invalid Details Failed to Login";
            //}

            return View();
        }
        [HttpPost]
        public ActionResult LoginPage(string UserName, String PWD)
        {
            
            if (UserName.ToLower().Trim() == "admin" && PWD == "nsabp")
            {
                Session["username"] = "Admin";
                Session["role"] = "Admin";                               
                return RedirectToAction("RCApp", "RCApplication", new { area = "RC" });               
            }
            //else if (UserName.ToLower().Trim() == "rcadmin" && PWD == "nsabp")
            //{
            //    Session["username"] = "RC Admin";
            //    Session["role"] = "RC Admin";
            //    return RedirectToAction("RC_Create", "RC");
            //}
            //else if (UserName.ToLower().Trim() == "padmin" && PWD == "nsabp")
            //{
            //    Session["username"] = "padmin";
            //    Session["role"] = "Protocol Manager";
            //    return RedirectToAction("ProtocolSetup", "ProtocolSetup");
            //}
           
            else
            {
                ViewBag.Message = "Login Failed due to invalid credentials";
                return View();
            }
           
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("LoginPage", "UserLogin");
        } 
    }
}