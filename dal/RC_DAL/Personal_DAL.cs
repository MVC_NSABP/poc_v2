﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace dal.RC_DAL
{
    public class Personal_DAL
    {
        public List<SP_GetRCPersonnelDetails_Result> GetRCPersonnelDetails()
        {
            using (var db = new POC_V2Entities())
            {
                List<SP_GetRCPersonnelDetails_Result> lstPersonaldetails = new List<SP_GetRCPersonnelDetails_Result>();
                try
                {
                    var result = db.SP_GetRCPersonnelDetails();
                   lstPersonaldetails = result.ToList();
                }
                catch (Exception ex)
                {
                }
                return lstPersonaldetails;
            }
        }

        public List<USP3_GetPersonRoleAddressesByPersonId_Result> GetPersonnelMappingAddressByPersonID(int PersonnelID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<USP3_GetPersonRoleAddressesByPersonId_Result> lstobj = new List<USP3_GetPersonRoleAddressesByPersonId_Result>();
                try
                {
                    var result = entity.USP3_GetPersonRoleAddressesByPersonId(PersonnelID);

                    lstobj = result.ToList();

                }
                catch (Exception ex)
                {
                }
                return lstobj;
            }
        }
    }
}
