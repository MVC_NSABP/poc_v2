﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Validation;

namespace dal.RC_DAL
{
    public class RCApp_DAL
    {
        public List<T206_RC_RegularActivities> GetRegularActivities()
        {
            using (var entity = new POC_V2Entities())
            {
                var ord = 0;
                List<T206_RC_RegularActivities> ListRC_Reg = new List<T206_RC_RegularActivities>();
                try
                {
                    var res = (from T206 in entity.T206_RC_RegularActivities
                               where T206.Record_status == "Active"
                               orderby ord ascending, T206.ID ascending
                               select new
                               {
                                   T206.ID,
                                   T206.Name,
                                   ord = T206.ID == 4 ? 1 : 0
                               }
                             ).ToList();

                    foreach (var item in res)
                    {
                        T206_RC_RegularActivities ob = new T206_RC_RegularActivities();
                        ob.Name = item.Name;
                        ob.ID = item.ID;
                        ListRC_Reg.Add(ob);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                return ListRC_Reg;
            }
        }
        public List<T203_RC_StudyPacrticipantActivities> GetStudyActivities()
        {
            using (var entity = new POC_V2Entities())
            {

                List<T203_RC_StudyPacrticipantActivities> ListStudyPart = new List<T203_RC_StudyPacrticipantActivities>();
                try
                {
                    var res = (from TM203 in entity.T203_RC_StudyPacrticipantActivities
                               where TM203.Record_status == "Active"
                               orderby TM203.ID ascending
                               select TM203);
                    ListStudyPart = res.ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return ListStudyPart;
            }


        }
        public List<T204_RC_DataManagementActivities> GetDataManagementActivities()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T204_RC_DataManagementActivities> ListRC_Data = new List<T204_RC_DataManagementActivities>();
                try
                {
                    var res = (from TM204 in entity.T204_RC_DataManagementActivities
                               where TM204.Record_status == "Active"
                               orderby TM204.ID ascending
                               select TM204);
                    ListRC_Data = res.ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return ListRC_Data;
            }


        }
        public List<T205_RC_PharmacyActivities> GetPharmacyActivities()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T205_RC_PharmacyActivities> PharmacyActivities = new List<T205_RC_PharmacyActivities>();
                try
                {
                    var res = (from TM205 in entity.T205_RC_PharmacyActivities
                               where TM205.Record_status == "Active"
                               orderby TM205.ID ascending
                               select TM205);
                    PharmacyActivities = res.ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return PharmacyActivities;
            }
        }
        public List<T201_RC_MedicalSpecialities> GetMedicalSpecialties()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T201_RC_MedicalSpecialities> PharmacyActivities = new List<T201_RC_MedicalSpecialities>();
                try
                {
                    var res = (from TM201 in entity.T201_RC_MedicalSpecialities
                               where TM201.Record_status == "Active"
                               orderby TM201.ID ascending
                               select TM201);
                    PharmacyActivities = res.ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return PharmacyActivities;
            }
        }
        public List<T209_Countries> getCountries()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T209_Countries> lstCountries = new List<T209_Countries>();
                try
                {
                    var res = (from TM002 in entity.T209_Countries
                               select new
                               {
                                   TM002.Country_Code,
                                   TM002.Country_Name
                               }
                             ).ToList();

                    foreach (var item in res)
                    {
                        T209_Countries ob = new T209_Countries();
                        ob.Country_Code = item.Country_Code;
                        ob.Country_Name = item.Country_Name;
                        lstCountries.Add(ob);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                return lstCountries;
            }
        }
        public List<T210_States> getStates(string Country_Code)
        {
            using (var entity = new POC_V2Entities())
            {
                List<T210_States> lstStates = new List<T210_States>();
                try
                {
                    var States = (from TM003 in entity.T210_States
                                  where TM003.T209__Country_Code.Equals(Country_Code)
                                  select new
                                  {
                                      TM003.State_Code,
                                      TM003.State_Name
                                  }
                             ).ToList();
                    foreach (var item in States)
                    {
                        T210_States ob = new T210_States();
                        ob.State_Code = item.State_Code;
                        ob.State_Name = item.State_Name;
                        lstStates.Add(ob);
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

                return lstStates;
            }
        }

        public string GetNxtRCID()
        {
            string rc_id = "";
            try
            {
                using (var entity = new POC_V2Entities())
                {

                    rc_id = entity.Database.SqlQuery<string>("select dbo.[getMaxSequentialID_RC]() as NextSID")
                            .FirstOrDefault();

                    //select dbo.[getMaxSequentialID_RC]() as NextSID
                }
            }
            catch (Exception)
            {

                throw;
            }
            return rc_id;
        }
        public Boolean IsRCIDExists(string rcID)
        {
            Boolean _result = false;
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    if (entity.T001_Research_Collaborator.Where(s => s.RCID.Contains(rcID) && s.Record_status == "Active").Count() > 0)
                        _result = true;
                    else
                        _result = false;
                }
            }
            catch (Exception ex)
            {
            }
            return _result;
        }
        public Boolean SaveRcApp(T001_Research_Collaborator T001)
        {
            Boolean _result = false;
            try
            {        
                using (var entity = new POC_V2Entities())
                {
                    entity.T001_Research_Collaborator.Add(T001);
                    entity.SaveChanges();
                   
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            return _result;
        }
        public List<T213_Address> lstGetAddresses()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T213_Address> lstAddress = new List<T213_Address>();
                try
                {
                    var res = (from T213_Address in entity.T213_Address
                               select T213_Address);

                    lstAddress = res.ToList();

                }
                catch (Exception ex)
                {

                }
                return lstAddress;
            }
        }
        public List<USP4_GetAddressesAssociationsByAddressId_Result> GetAddressMappingPersonSiteByAddressID(int AddressID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<USP4_GetAddressesAssociationsByAddressId_Result> lstobj = new List<USP4_GetAddressesAssociationsByAddressId_Result>();
                try
                {
                    var result = entity.USP4_GetAddressesAssociationsByAddressId(AddressID);

                    lstobj = result.ToList();


                }
                catch (Exception ex)
                {
                }
                return lstobj;
            }
        }
    }
}
