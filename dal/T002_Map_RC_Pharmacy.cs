//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    using System.Collections.Generic;
    
    public partial class T002_Map_RC_Pharmacy
    {
        public int ID { get; set; }
        public int T001_RCID { get; set; }
        public int T205_PharmacyActivityID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string Record_status { get; set; }
    
        public virtual T001_Research_Collaborator T001_Research_Collaborator { get; set; }
        public virtual T205_RC_PharmacyActivities T205_RC_PharmacyActivities { get; set; }
    }
}
