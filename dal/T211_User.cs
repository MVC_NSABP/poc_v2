//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    using System.Collections.Generic;
    
    public partial class T211_User
    {
        public int User_ID { get; set; }
        public string User_FName { get; set; }
        public string User_LName { get; set; }
        public string User_Email { get; set; }
        public string User_Phone { get; set; }
        public string User_Name { get; set; }
        public string User_Password { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_On { get; set; }
        public string Record_Status { get; set; }
        public Nullable<int> T208_Status_ID { get; set; }
    }
}
